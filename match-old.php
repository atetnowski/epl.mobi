<?php
$game_id = intval($_GET['match_id']);

$commentaryJSON = file_get_contents('data/epl/commentary/' . $game_id . '.txt');
$matchInfoJSON = file_get_contents('data/epl/match-info/' . $game_id . '.txt');
$gifJSON = file_get_contents('data/epl/gifs/' . $game_id . '.txt');

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
<title>:: EPL Scores ::</title>
<meta name="viewport" content="width=device-width, user-scalable=no">

<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700,900' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/swiper.css" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/swiper.js"></script>
</head>
<body>
<header onclick="navClick()">
	<div class="container">
		<h1><a href="index.php">EPL<span class="alt">.MOBI</span></a></h1>
		<a href="javascript: return false;" class="nav_button menu_button"><i class="fa fa-bars"></i></a>
	</div>
</header>
<nav>
	<div class="container">
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-clock-o"></i>
				<span>Live</span>
			</a>
		</div>
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-calendar"></i>
				<span>Calendar</span>
			</a>
		</div>
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-align-justify"></i>
				<span>Table</span>
			</a>
		</div>
	</div>
</nav>
<div class="swiper-container">
	<div class="swiper-wrapper">
		<!--First Slide-->
		<div class="swiper-slide">
			<div class="page_header">
				<h3>Live Score</h3>
			</div>
			<?php
			if($matchInfoJSON == ''){

				$html  = '<section class="match no_match">';
				$html .= '<div class="container">';
				$html .= '<p class="no_match_text">We cannot seem to find any data on the match you requested. If you think this is a mistake, please click <a href="#">here</a> to let us know.</p>';
				$html .= '</div>';
				$html .= '</section>';

				echo $html;

			} else {
				$matchData = json_decode($matchInfoJSON);
				
				$html  = '<div class="match" >';
				$html .= '<div class="container">';
				$html .= '<div class="score_container home ' . strtolower($matchData->match_localteam_name) . '">';
				$html .= '<p class="score">' . $matchData->match_localteam_score . '</p>';
				$html .= '<p class="team">' . $matchData->match_localteam_name . '</p>';
				$html .= '</div>';
				$html .= '<div class="match_data_container">';
				$html .= '<p class="minute">FT</p>';
				$html .= '</div>';
				$html .= '<div class="score_container away ' . strtolower($matchData->match_visitorteam_name) . '">';
				$html .= '<p class="score">' . $matchData->match_visitorteam_score . '</p>';
				$html .= '<p class="team">' . $matchData->match_visitorteam_name . '</p>';
				$html .= '</div>';
				$html .= '</div>';
				$html .= '</div>';

				echo $html;

				echo '<div class="match_events">';
				echo '<h5>Match Events</h5>';
				foreach($matchData->match_events as $event){

					if($event->event_team == 'localteam'){
						$thisTeamName = $matchData->match_localteam_name;
					} else {
						$thisTeamName = $matchData->match_visitorteam_name;
					}
					
					if($event->event_type == 'goal'){
						$icon = '<i class="fa fa-futbol-o"></i>';
					} else if ($event->event_type == 'yellowcard'){
						$icon = '<i class="fa fa-square yellow"></i>';
					} else if ($event->event_type == 'redcard'){
						$icon = '<i class="fa fa-square red"></i>';
					}

					if($event->event_team == 'localteam'){
						$eventHTML  = '<div class="match_event_container home">';
						$eventHTML .= '<p class="event_player">' . $event->event_player . $icon . '</p>';
						$eventHTML .= '<div class="event_minute">' . $event->event_minute . '</div>';
						$eventHTML .= '</div>';
					} else{
						$eventHTML  = '<div class="match_event_container away">';
						$eventHTML .= '<div class="event_minute">' . $event->event_minute . '</div>';
						$eventHTML .= '<p class="event_player">' . $icon . $event->event_player .'</p>';
						$eventHTML .= '</div>';
					}

					echo $eventHTML;
				}
				echo '</div>';
			}
			?>
		</div>
		<div class="swiper-slide">
			<div class="page_header">
				<h3>Commentary</h3>
			</div>
			<?php
			if($commentaryJSON == ''){

				$html  = '<section class="match no_match">';
				$html .= '<div class="container">';
				$html .= '<p class="no_match_text">We cannot seem to find any commentary on the match you requested. If you think this is a mistake, please click <a href="#">here</a> to let us know.</p>';
				$html .= '</div>';
				$html .= '</section>';

				echo $html;

			} else {
				$data = json_decode($commentaryJSON);

				//print_r($data);
				$comments = $data->commentaries[0]->comm_commentaries->comment;
				$commentsArray = get_object_vars($comments);
				

				function cmp($a, $b){
				    return strcmp($b->id, $a->id);
				}

				usort($commentsArray, "cmp");

				echo '<section class="commentary">';
				
				foreach($commentsArray as $comment){

					$html  = '<div class="comment_container">';
					$html .= '<p class="comment_minute">' . $comment->minute . '</p>';
					$html .= '<p class="comment_text">' . $comment->comment . '</p>';
					$html .= '</div>';

					echo $html;
				}
				echo '</section>';



			}
			?>
		</div>
		<div class="swiper-slide">
			<div class="page_header">
				<h3>Goals</h3>
			</div>
			<?php
			if($gifJSON == ''){

				$html  = '<section class="match no_match">';
				$html .= '<div class="container">';
				$html .= '<p class="no_match_text">We cannot seem to find any data on the match you requested. If you think this is a mistake, please click <a href="#">here</a> to let us know.</p>';
				$html .= '</div>';
				$html .= '</section>';

				echo $html;

			} else {
				$data = json_decode($gifJSON);

				foreach ($data as $goal){

					$html  = '<video width="320" height="240" controls>';
					$html .= '<source src="' . $goal->link . '" type="video/mp4">';
					$html .= 'Your browser does not support the video tag.';
					$html .= '</video>';

					echo $html;
				}

			}
			?>
		</div>
	</div>
</div>
<div class="match_spacer"></div>
<footer>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- epl.mobi -->
	<ins class="adsbygoogle"
	     style="display:inline-block;width:320px;height:50px"
	     data-ad-client="ca-pub-7683067780523454"
	     data-ad-slot="1031772742"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54513674-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
function navClick(){
	$('nav').toggleClass('open');
}
</script>

<script type="text/javascript">

$(function(){
	var mySwiper = $('.swiper-container').swiper({
		mode:'horizontal',
		loop: true
	});
})

</script>


</body>
</html>
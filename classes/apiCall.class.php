<?php
class ApiCall{
	private $_apiKey;
	private $_hasError = 0;
	private $_errorText = array();
	private $_output;
	private $_outputType;

	public function __construct($apiKey){
		$this->_apiKey = $apiKey;
		
		// Method to test that the API key is valid.
		// Cant use this right now as this counts against the rate limit, which is very low already

		// $this->testApiKey();
	}

	private function testApiKey(){
		$this->getLeagues();
		$json = $this->_output;
		$data = json_decode($json);
		$status = $data->ERROR;

		if($status !== 'OK'){
			$this->setError('API Key is invalid');
		}
	}

	public function getCommentary($matchId){
		$this->checkErrors();

		$url = "http://football-api.com/api/?Action=commentaries&APIKey=" . $this->_apiKey . "&match_id=" . $matchId;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
		if(!$json) {
		    echo curl_error($ch);
		}
		curl_close($ch);

		$this->_output = $json;
		$this->_outputType = 'commentaries';
	}

	public function getLeagues(){
		$this->checkErrors();

		$url = "http://football-api.com/api/?Action=competitions&APIKey=" . $this->_apiKey;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
		if(!$json) {
		    echo curl_error($ch);
		}
		curl_close($ch);

		$this->_output = $json;
		$this->_outputType = 'competitions';
	}

	public function getGamesByDate($compId, $date){
		$this->checkErrors();

		$url = "http://football-api.com/api/?Action=fixtures&APIKey=" . $this->_apiKey . "&comp_id=" . $compId . "&match_date=" . $date;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
		if(!$json) {
		    echo curl_error($ch);
		}
		curl_close($ch);

		//Checks that there are games being returned
		$thisReturn = json_decode($json);

		if($thisReturn->ERROR == 'no matches found in that day'){
			$this->_output = ''; 
		} else {
			$this->_output = $json;
			$this->_outputType = 'fixtures';
		}

		
	}

	public function getLeagueStandings($compId){
		$this->checkErrors();

		$url = "http://football-api.com/api/?Action=standings&APIKey=" . $this->_apiKey . "&comp_id=" . $compId;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
		if(!$json) {
		    echo curl_error($ch);
		}
		curl_close($ch);

		$this->_output = $json;
		$this->_outputType = 'standings';
	}

	public function getTodaysGames($compId){
		$this->checkErrors();

		$url = "http://football-api.com/api/?Action=today&APIKey=" . $this->_apiKey . "&comp_id=" . $compId;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
		if(!$json) {
		    echo curl_error($ch);
		}
		curl_close($ch);

		echo $json;
		
		$this->_output = $json;
		$this->_outputType = 'today';
	}

	/* Log functions */
	public function returnOutput(){
		return $this->_output;
	}
	public function makeFile($fileName){
		if($fileName == ''){
			$this->setError('Please enter a filename without the extension');
		} else if($this->_output == ''){
			$this->setError('You have not called any data to be written.');
		} else {
			file_put_contents($fileName . '.txt',$this->_output);
		}
	}
	public function writeMatchInfo($folder){
		if( $this->_outputType == 'fixtures' ||
			$this->_outputType == 'today'){
			$data = json_decode($this->_output);

		print_r($data);

			foreach($data->matches as $match){
				print_r($match);
				echo '<br><br>';

				$match_id = $match->match_id;
				$thisMatchJSON = json_encode($match);
				echo '<br><br>';

				echo $thisMatchJSON;

				#TODO: check if folder exists
				file_put_contents($folder . $match_id . '.txt',$thisMatchJSON);
			}
		} else {
			$this->setError('Cannot write match info because output data is not fixtures.');
		}
	}

	//Error Handeling Methods
	private function setError($errorText){
		$this->_hasError = 1;
		array_push($this->_errorText, $errorText);
	}
	private function checkErrors(){
		if($this->_hasError == 1){
			$errorJSON = json_encode($this->_errorText);
			exit($errorJSON);
		}
	}
}
?>
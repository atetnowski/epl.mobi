<?php
/* 
** This cron should be run atleast every 15 seconds while a game is in progress
** This will save the JSON for this game id
** games will be saved in /data/epl/commentary/{game_id}.txt
*/

require('../classes/apiCall.class.php');

#TODO - This needs validation
$game_id = intval($_GET['game_id']);
if(intval($game_id) < 1){
	exit('Invalid Game ID.');
}

$api = new ApiCall('6ea81522-cb5b-afc9-1134137ca8ab');
$api->getCommentary($game_id); 
$api->makeFile('../data/epl/commentary/' . $game_id);

?>
<?php
/* 
** This cron should be run while any games are live
** Returns live scores for today
** games will be saved in /data/epl/live-scores/yyyy-mm-dd.txt
*/

require('../classes/apiCall.class.php');

$tempDate = date("d.m.Y");
$tempDateUrl = date("Y-m-d");

$api = new ApiCall('6ea81522-cb5b-afc9-1134137ca8ab');
$api->getTodaysGames('1204'); //EPL is 1204
$api->makeFile('../data/epl/live-scores/' . $tempDateUrl);
$api->writeMatchInfo('../data/epl/match-info/');

?>
<?php
/* 
** This cron should be run daily at midnight
** This will save the JSON for the next week's worth of upcoming games
** games will be saved in /data/epl/games-by-date/yyyy-mm-dd.txt
*/

require('../classes/apiCall.class.php');

for($i = -3; $i <= 7; $i++){
	$tempDate = date("d.m.Y",strtotime("+" . $i . " day"));
	$tempDateUrl = date("Y-m-d",strtotime("+" . $i . " day"));

	$api = new ApiCall('6ea81522-cb5b-afc9-1134137ca8ab');
	$api->getGamesByDate('1204',$tempDate); //EPL is 1204 - Date format dd.mm.yyyy
	$api->makeFile('../data/epl/games-by-date/' . $tempDateUrl);
	$api->writeMatchInfo('../data/epl/match-info/');
}

?>
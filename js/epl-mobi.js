
function View(){
	self = this;

	self.throwError = function(error){
		alert(error);
	}
	self.getTodaysDate = function(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();

		if(dd < 10){
			dd = '0' + dd;
		}
		if(mm < 10){
			mm = '0' + mm;
		}

		return yyyy + '-' + mm + '-' + dd;
	}
}
function LiveScore(date){
	self = this;
	self.date = date || self.getTodaysDate();
	self.json;

	self.getJSON = function(){
		$.ajax({
			type: "GET",
			url: '../data/epl/games-by-date/' + self.date + '.txt',
			
		}).done(function( msg ) {
			self.json = JSON.parse(msg);
		}).fail(function(){
			self.noJSON();
		});
	}

	self.noJSON = function(){
		console.log('No JSON');
	}
}

LiveScore.prototype = new View();

var a = new LiveScore('2014-08-31');
a.getJSON();
var b = new LiveScore();
b.getJSON();

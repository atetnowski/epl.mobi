<?php
error_reporting(1);
$date = date('Y-m-d');
$date = '2014-08-31';

$json = file_get_contents('data/epl/games-by-date/' . $date . '.txt');

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
<title>:: EPL Scores ::</title>
<meta name="viewport" content="width=device-width, user-scalable=no">

<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700,900' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/Chart.min.js"></script>
<script src="js/clndr.js"></script>
</head>
<body>
<header onclick="navClick()">
	<div class="container">
		<h1><a href="index.php">EPL<span class="alt">.MOBI</span></a></h1>
		<a href="javascript: return false;" class="nav_button menu_button"><i class="fa fa-bars"></i></a>
	</div>
</header>
<section class="welcome">
	<div class="container">
		<h1>EPL Mobile</h1>
		<h2>The easiest app for keeping track of live English Premier League scores</h2>
	</div>
</section>
<nav>
	<div class="container">
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-clock-o"></i>
				<span>Live</span>
			</a>
		</div>
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-calendar"></i>
				<span>Calendar</span>
			</a>
		</div>
		<div class="menu_item">
			<a href="#">
				<i class="fa fa-align-justify"></i>
				<span>Table</span>
			</a>
		</div>
	</div>
</nav>


<div id="calendar"></div>



<div class="match_spacer"></div>
<footer>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- epl.mobi -->
	<ins class="adsbygoogle"
	     style="display:inline-block;width:320px;height:50px"
	     data-ad-client="ca-pub-7683067780523454"
	     data-ad-slot="1031772742"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</footer>
<!-- Template HTML

<section class="match" data-matchId="1931982">
	<div class="container">
		<div class="score_container">
			<p class="score">3</p>
			<p class="team">Everton</p>
		</div>
		<div class="match_data_container">
			<p class="minute">FT</p>
		</div>
		<div class="score_container">
			<p class="score">6</p>
			<p class="team">Chelsea</p>
		</div>
	</div>
</section>
-->
<script>
$(function(){
	$('#calendar').clndr();
});

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54513674-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
function navClick(){
	$('nav').toggleClass('open');
}
</script>

</body>
</html>